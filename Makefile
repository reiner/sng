# Makefile for SNG
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERSION=$(shell sed -n <NEWS.adoc '/::/s/^\([0-9][^:]*\).*/\1/p' | head -1)

RGBTXT=/usr/share/X11/rgb.txt

.PATH: $(.PARSEDIR)
prefix?=/usr/local
target=$(DESTDIR)$(prefix)
parsedir:=$(.PARSEDIR)
srcdir=$(dir $(abspath $(firstword $(MAKEFILE_LIST))))$(parsedir)
VPATH=$(srcdir)
mandir?=$(DESTDIR)$(prefix)/share/man

INSTALL = install

GCC_WARNINGS1=-Wall -Wpointer-arith -Wstrict-prototypes
GCC_WARNINGS2=-Wmissing-prototypes -Wmissing-declarations
GCC_WARNINGS3=-Wno-unused-function -Wno-unused-label -Wno-format-zero-length
GCC_WARNINGS=$(GCC_WARNINGS1) $(GCC_WARNINGS2) $(GCC_WARNINGS3)
CFLAGS += $(GCC_WARNINGS)
CPPFLAGS += -I. -I$(srcdir)
#LIBS=-lrt
CPPFLAGS += -DVERSION=\"$(VERSION)\" -DRGBTXT=\"$(RGBTXT)\"

# First line works for GNU C.  
# Replace with the next if your compiler doesn't support C99 restrict qualifier
CPPFLAGS+=-Drestrict=__restrict__
#CPPFLAGS+=-Drestrict=""

# To enable profiling, uncomment the following line
# Note: the profiler gets confused if you don't also turn off -O flags.
# CFLAGS += -pg
# Warning: Using -O3 has been seen to cause core dumps on repositories with
# very long revision names - some bounds check gets optimized away. Don't do that.
# CFLAGS += -O2
# If your toolchain supports link time optimization this is a cheap speedup
# CFLAGS += -flto
CFLAGS += -g
# Test coverage flags
# CFLAGS += -ftest-coverage -fprofile-arcs
CFLAGS += $(EXTRA_CFLAGS)

LDFLAGS= -lpng

OBJS= main.o sngc.o sngd.o

all: sng man html

sng: $(OBJS)
	$(CC) $(CFLAGS) $(TARGET_ARCH) $(OBJS) $(LDFLAGS) $(LIBS) -o $@

$(OBJS): sng.h Makefile

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

docdebug:
	xmllint --nonet --noout --valid sng.xml

man: sng.1

html: sng.html

clean:
	rm -f $(OBJS) sng
	rm -f *.1 *.html gmon.out
	rm -f MANIFEST index.html *.tar.gz
	rm -f *.gcno *.gcda

shellcheck:
	@-shellcheck -s sh -f gcc sng_regress

# Regression-test sng.  Passes if no differences show up.
# Assumes we have a copy of Willem van Schaik's PNG test suite under pngsuite
check: sng shellcheck
	@./sng_regress test.sng pngsuite/[a-wyz]*.png
	@echo "No output is good news."

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

fixme:
	@if command -v rg; then \
		rg --no-heading FIX''ME; \
	else \
		find . -type f -exec grep -n FIX''ME {} /dev/null \; | grep -v "[.]git"; \
	fi

install: install-bin install-man
install-bin: sng
	$(INSTALL) -d "$(target)/bin"
	$(INSTALL) $^ "$(target)/bin"
install-man: man
	$(INSTALL) -d "$(mandir)/man1"
	$(INSTALL) -m 644 sng.1 "$(mandir)/man1"
uninstall: uninstall-man uninstall-bin
uninstall-man:
	cd $(mandir)/man1/ && rm -f sng.1
uninstall-bin:
	cd $(target)/bin && rm -f sng

version:
	@echo $(VERSION)

cppcheck:
	cppcheck -I. --template=gcc --enable=all --suppress=missingIncludeSystem --inline-suppr *.[ch]

SOURCES = Makefile *.[ch]
DOCS = control *.adoc snglogo.png
ALL =  $(SOURCES) $(DOCS)
sng-$(VERSION).tar.gz: $(ALL) sng.1
	(git ls-files; ls *.1) | sed s:^:sng-$(VERSION)/: >MANIFEST
	(cd ..; ln -s sng sng-$(VERSION))
	(cd ..; tar -cJf sng/sng-$(VERSION).tar.xz `cat sng/MANIFEST`)
	(cd ..; rm sng-$(VERSION) sng/MANIFEST)

dist: clean sng-$(VERSION).tar.gz

release: sng-$(VERSION).tar.gz html
	shipper version=$(VERSION) | sh -e -x

refresh: html
	shipper -N -w version=$(VERSION) | sh -e -x

# end
