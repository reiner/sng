'\" t
.\"     Title: sng
.\"    Author: [see the "AUTHOR(S)" section]
.\" Generator: Asciidoctor 2.0.16
.\"      Date: 2024-02-09
.\"    Manual: \ \&
.\"    Source: \ \&
.\"  Language: English
.\"
.TH "SNG" "1" "2024-02-09" "\ \&" "\ \&"
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.ss \n[.ss] 0
.nh
.ad l
.de URL
\fI\\$2\fP <\\$1>\\$3
..
.als MTO URL
.if \n[.g] \{\
.  mso www.tmac
.  am URL
.    ad l
.  .
.  am MTO
.    ad l
.  .
.  LINKSTYLE blue R < >
.\}
.SH "NAME"
sng \- compiler/decompiler for Scriptable Network Graphics
.SH "SYNOPSIS"
.sp
\fBcsng\fP [file...]
.SH "DESCRIPTION"
.sp
The sng program translates between PNG (Portable Network Graphics)
format and SNG (Scriptable Network Graphics) format.  SNG is a
printable and editable minilanguage for describing PNG files.  With
sng, it is easy to view and edit exotic PNG chunks not supported by
graphics editors; also, since SNG is easy to generate from scripts,
sng may be useful at the end of a pipeline that programmatically
generates PNG images.
.sp
An SNG description consists of a series of chunk specifications in a
simple editable text format.  These generally correspond one\-for\-one
to PNG chunks.  There is one exception; the IMAGE chunk
specification is automatically translated into an IDAT chunk (doing
appropriate interlacing, compression, etcetera).
.sp
Given no file arguments, sng translates stdin
to stdout.  In this mode, it checks the first character. If that
character is printable, the input stream is assumed to contain SNG;
sng looks for an #SNG leader and tries to translate
the file to PNG.  If the character is non\-printable, the input stream
is assumed to contain PNG; sng tries to translate
it to SNG.
.sp
For each file that sng operates on, it does
its conversion according to the file extension (.png or .sng).  The
result file has the same name left of the dot as the original, but the
opposite extension and type.
.sp
The \-V option makes sng identify itself and its version, then exit.
.sp
The \-v option makes sng report on what files it is converting.
.SH "SNG LANGUAGE SYNTAX"
.sp
In general, the SNG language is token\-oriented with tokens separated
by whitespace.  Anywhere that whitespace may appear, a "#" comment
leader causes all characters up to the next following newline to be
ignored.  The characters ":" and ";" are treated as whitespace, except
the ";" terminates a data element (see below).
.sp
In the syntax descriptions below, lines between {} may occur in any
order. Elements bracketed in [] are optional; a sequence bracketed by
[]* may be repeated any number of times.  Elements separated by | are
alternatives.  Elements separated by plus signs are an attribute set;
any sequence of one or more of those element tokens is valid.
.sp
The elements <byte>, <short>, <long>,
<float>, <string> are byte numeric, short\-integer numeric,
long\-integer numeric, and float numeric literals respectively (all
unsigned).  The <slong> element is a signed long\-numeric
literal. All numerics use C conventions; that is, they are decimal
unless led by 0x (hex) or 0 (octal).
.sp
The element <string> is any number of doublequote\-delimited character
string literals.  C\-style escapes (\(rsn, \(rst, \(rsb, \(rsr or \(rs followed by
octal or hex digits) are interpreted.  The result is the concatenation
of all the literals.
.sp
The element <keyword> is a doublequote\-delimited PNG
keyword; that is, a string of no more than 79 printable Latin\-1
characters or spaces, with no leading and no trailing and no
consecutive spaces.
.sp
A <data> element consists of a sequence of byte specifications in any
of the following formats. Either \*(Aq}\*(Aq or \*(Aq;\*(Aq ends a data literal; \*(Aq}\*(Aq
also ends the enclosing chunk.
.sp
.RS 4
.ie n \{\
\h'-04' 1.\h'+01'\c
.\}
.el \{\
.  sp -1
.  IP " 1." 4.2
.\}
\fIstring\fP format is an SNG string literal or sequence of string
literals (see above). The bytes of data are the string contents.
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 2.\h'+01'\c
.\}
.el \{\
.  sp -1
.  IP " 2." 4.2
.\}
\fIbase64\fP format is signaled by the leading token \*(Aqbase64\*(Aq.  This
encoding can only be used when the values of all bytes are less than 64.
It encodes each byte as a single character, with decimal digits
representing values 0\-9, followed by A\-Z for 10\-35, followed by a\-z
for 36\-61, followed by + for 62 and / for 63.  Base64 format can be
used if the image either has total (color plus alpha) bit depth of
four or less, or it is a spaletted image with 64 or fewer colors.
Whitespace is ignored.  Note that this encoding is only very loosely
related to RFC2045 base\-64 encoding, which uses a different mapping of
bytes to values, and supports encoding of arbitrary binary data.
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 3.\h'+01'\c
.\}
.el \{\
.  sp -1
.  IP " 3." 4.2
.\}
\fIhex\fP format is signaled by the leading token "hex". In hex format,
each byte is specified by two hex digits (0123456789abcdef), most
significant first.  Whitespace is ignored.
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 4.\h'+01'\c
.\}
.el \{\
.  sp -1
.  IP " 4." 4.2
.\}
\fIP1\fP format is Portable Bit Map (PBM) format P1.  A decimal height
and width follow; it is a fatal error for them to fail to match the
IHDR dimensions.  Following this, the only non\-whitespace characters
are expected to be "0" and "1", with the obvious values.  Whitespace
is ignored.
.RE
.sp
.RS 4
.ie n \{\
\h'-04' 5.\h'+01'\c
.\}
.el \{\
.  sp -1
.  IP " 5." 4.2
.\}
\fIP3\fP format is Portable Pixel Map (PPM) format P3.  A decimal
height and width follow; it is a fatal error for them to fail to match
the IHDR dimensions.  A maximum channel value in decimal follows; it
is a fatal error for any following channel value to exceed this value.
Following this are triples of decimal channel values representing RGB
triples.  Whitespace separates decimal channel values but is otherwise
ignored.
.RE
.sp
An <rgb> element may be expanded to:
.sp
.ce
\l'\n(.lu*25u/100u\(ap'
.sp
(<byte>, <byte>, <byte>) | <string>
\-\-\-
.sp
That is, it is either a paren\-enclosed list of RGB values or a string
naming a color named in the X RGB database.  Note that color names are
not necessarily portable between hosts or even displays, due to
different screen gammas and colorimetric biases.  For this
reason, the SNG decompiler generates color names in comments.
.sp
IMAGE segments contain unpacked and uninterlaced raster data.  There
will be exactly one IMAGE per SNG dump, containing the pixel data from
all IDAT chunks, unless the \-i option is on.  In that case, there will
be multiple IDAT chunks containing raw (compressed) image data.
.sp
The options member of an IMAGE chunk (if present) sets image write
transformations, supplying the third argument of the png_write_png()
call used for output.  Note that for images with a bit depth of less
than 8, there is a default "packing" transformation.  Consult the
libpng(3) manual page for details.
.sp
Every SNG file must begin with the string "#SNG", followed by optional
SNG version information, followed by a colon (":", ASCII 58)
character.  The remainder of the first line is ignored by SNG.
.sp
Comments in the syntax diagram describe intended semantics.  This
specification should be read in conjunction with the PNG standard.
.sp
.ce
\l'\n(.lu*25u/100u\(ap'
.sp
IHDR {
height <long>
width <long>
bitdepth <byte>
[using grayscale+color+palette+alpha]
[with interlace]			# Adam7 assumed if interlacing on
}
.sp
PLTE {
[<rgb>]*				# RGB triples or X color names
}
.sp
IDAT {
<data>
}
.sp
gAMA {<float>}
.sp
cHRM {
white (<float>,<float>)      # White point x and y
red (<float>,<float>)
green (<float>,<float>)
blue (<float>,<float>)
}
.sp
sRGB {<byte>}                   # Colorimetry intent, range 0\-3
.sp
iCCP {                          # International Color Consortium profile
name <keyword>
profile <data>
}
.sp
sBIT {
red <byte>                   # Color images only
blue <byte>                  # Color images only
green <byte>                 # Color images only
gray <byte>                  # Grayscale images only
alpha <byte>                 # Images with alpha only
}
.sp
bKGD {
red <short>                  # Color images only
blue <short>                 # Color images only
green <short>                # Color images only
gray <short>                 # Grayscale images only
index <byte>                 # Paletted images only
}
.sp
hIST {
<short> [, <short>]*         # Count must match palette size
}
.sp
tRNS {
[gray <short>]               # Grayscale images only
[red <short>]                # True\-color images only
[green <short>]              # True\-color images only
[blue <short>]               # True\-color images only
[<byte>]*                    # Paletted images only
}
.sp
pHYs {
xpixels <long>
ypixels <long>
[per meter]
}
.sp
tIME {
year <short>
month <byte>
day <byte>
hour <byte>
minute <byte>
second <byte>
}
.sp
tEXt {                          # Ordinary text chunk
keyword <keyword>
text <string>
}
.sp
zTXt {                          # Compressed text chunk
keyword <keyword>
text <string>
}
.sp
iTXt {                          # International UTF\-8 keyword
language <keyword>
keyword <keyword>
translated <keyword>          # Translation of the keyword
text <string>
[compressed]
}
.sp
oFFs {
xoffset <slong>
yoffset <slong>
[unit pixels|micrometers]*
}
.sp
sPLT {
name <keyword>
depth <byte>
[<rgb>, <short>, <short>]*    # Color followed by alpha and frequency
}
.sp
pCAL {
name <keyword>
x0 <slong>
x1 <slong>
mapping linear|euler|exponential|hyperboli   unit <string>
[parameters <string>]
}
.sp
sCAL {
unit meter|radian
width <string>
height <string>
}
.sp
IMAGE {
options identity+packing+packswap+invert_mono
+shift+bgr+swap_alpha+invert_alpha+swap_endian+strip_filler
pixels <data>
}
.sp
gIFg {
disposal <byte>
input <byte>
delay <short>
}
.sp
gIFx {
identifier <string>                # Must be 8 characters
code <string>                      # Must be 3 characters
data <data>
}
.sp
private <string> {                    # Private chunk declaration
<data>
}
\-\-\-
.SH "BUGS"
.sp
The \-i option doesn\(cqt work yet, and won\(cqt until libpng\(cqs ability to
suppress special handling of IDATs is working.  See the
distribution TODO file for other minor problems.
.SH "FILES"
.sp
rgb.txt
.RS 4
The X colorname database, used for RGB\-to\-name mappings in the
decompiler and name\-to\-RGB mappings in the compiler.
.RE
.SH "SEE ALSO"
.sp
pbm(5), ppm(5).
.SH "AUTHOR"
.sp
Eric S. Raymond \c
.MTO "esr\(atsnark.thyrsus.com" "" ""
December 1999.
The SNG home page is at \c
.URL "http://sng.sourceforge.net/" "" ""
.sp
For more information about PNG, see the
PNG website at \c
.URL "http://www.libpng.org/pub/png/" "" ""
.sp
The W3C recommendation is
\c
.URL "http://www.w3.org/TR/2003/PR\-PNG\-20030520/index\-noobject.html" "
Portable Network Graphics (PNG) Specification (Second Edition)" .  The
PNG specification is also ISO/IEC 15948.